export default { 
  minHeight: `300px`, 
  maxHeight: `600px`,
  overflowY: 'auto', 
  overflowX: 'hidden' 
}