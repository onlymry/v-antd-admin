import engine from "store/src/store-engine";

import localStorage from "store/storages/localStorage";
import sessionStorage from "store/storages/sessionStorage";
import cookieStorage from "store/storages/cookieStorage";


export default class Tools {
  // 判断是否是浏览器环境
  public inBrowser = typeof window !== 'undefined';
  // 获取浏览器ua
  public UA = this.inBrowser && window.navigator.userAgent.toLowerCase();
  // 当前浏览器是否是ie
  public isIE = this.UA && /msie|trident/.test(this.UA);
  //当前浏览器是ie9
  public isIE9 = this.UA && this.UA.indexOf('msie 9.0') > 0;
  // 当前浏览器是edge
  public isEdge = this.UA && this.UA.indexOf('edge/') > 0;
  //当前浏览器是android
  public isAndroid = (this.UA && this.UA.indexOf('android') > 0);
  // 当前浏览器是ios
  public isIOS = (this.UA && /iphone|ipad|ipod|ios/.test(this.UA));
  // 当前浏览器是chrome
  public isChrome = this.UA && /chrome\/\d+/.test(this.UA) && !this.isEdge;
  // 当前环境是phantomjs
  public isPhantomJS = this.UA && /phantomjs/.test(this.UA);
  // 当前浏览器是火狐
  public isFF = this.UA && this.UA.match(/firefox\/(\d+)/);
  // 当前浏览器对象是否存在__proto__
  public hasProto = '__proto__' in {};
  // store插件--localStorage
  public localStore = engine.createStore(localStorage);
  // store插件--sessionStorage
  public sessionStore = engine.createStore(sessionStorage);
  // store插件--cookieStorage
  public cookieStore = engine.createStore(cookieStorage);
  // 判断变量是否是undefined
  public isUndef(v: any) {
    return v === undefined || v === null
  }
  //判断变量是否已经定义
  public isDef(v: any) {
    return v !== undefined && v !== null
  }
  // 判断变量是否为真
  public isTrue(v: any) {
    return v === true
  }
  // 判断变量是否为假
  public isFalse(v: any) {
    return v === false
  }
  // 判断变量是否是对象
  public isObject(obj: any) {
    return obj !== null && typeof obj === 'object'
  }
  // 判断变量是否是正则
  public isRegExp(v: any) {
    return Object.prototype.toString.call(v) === '[object RegExp]'
  }
  // 判断变量是否是Promise
  public isPromise(val: any) {
    return (
      this.isDef(val) &&
      typeof val.then === 'function' &&
      typeof val.catch === 'function'
    )
  }
  // 转化number
  public toNumber(val: any, type: string) {
    if (this.isUndef(type) || type === "float" || type === "double") {
      let n = parseFloat(val);
      return isNaN(n) ? val : n;
    } else {
      let n = parseInt(val);
      return isNaN(n) ? val : n;
    }
  }

  // 判空
  public isEmpty(val: any) {
    let type = typeof val;
    // 是对象或者json
    if (JSON.stringify(val) === "{}" && type === "object") return true;
    // 是数字
    if (type === "number") return false;
    // null
    if (type === "object" && val === null) return true;
    // string
    if (type === "string" && val.length === 0) return true;
    // undefined
    if (type === 'undefined' && val === undefined) return true;
    // array
    if (type === 'object' && JSON.stringify(val) === "[]" && val.length === 0) return true;

    return false;
  }
  // get localstroage
  public getLocalStroage(key: string) {
    if (!this.inBrowser) {
      return {
        status: 0,
        value: '',
        error: new Error('not in browser')
      };
    } else {
      try {
        return {
          status: 1,
          value: window.localStorage.getItem(key),
          error: null
        }
      } catch (error) {
        return {
          status: 0,
          value: '',
          error: error
        }
      }
    }
  }
  // set localstroage
  public setLocalStroage(key: string, value: string) {
    if (!this.inBrowser) {
      return {
        status: 0,
        value: '',
        error: new Error('not in browser')
      };
    } else {
      try {
        window.localStorage.setItem(key, value);
      } catch (error) {
        return {
          status: 0,
          value: '',
          error: error
        }
      }
    }
  }

  // set async localstroage
  // 同步localstroage
  public async setAsyncLocalStroage(key: string, value: string) {
    if (!this.inBrowser) {
      return await {
        status: 0,
        value: '',
        error: new Error('not in browser')
      };
    } else {
      try {
        window.localStorage.setItem(key, value);
        return await this.getLocalStroage(key);
      } catch (error) {
        return await {
          status: 0,
          value: '',
          error: error
        }
      }
    }
  }
}