export default class Config {
  public debug = false;
  public devUrl = '';
  public proUrl = '';
  public currentUrl = this.debug ? this.devUrl : this.proUrl;
}

