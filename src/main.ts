import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router';
import Antd from 'ant-design-vue';

import store from './store'
import 'ant-design-vue/dist/antd.css';

import Config from "@/tools/debug.config";



Vue.config.productionTip = false
Vue.use(Antd);
Vue.prototype.debugMode = new Config();
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
