import {Module} from 'vuex';
import {UserState} from './type';
import {actions} from './action';
import {getters} from './getter';
import {mutations} from './mutation';
import {RootState} from '../type'

// 初始state
const state:UserState = JSON.parse(sessionStorage.getItem('userinfo')||'{}')

const namespaced = true;

export const user:Module<UserState,RootState> = {
  namespaced,
  state,
  getters,
  mutations,
  actions
}


export default state;