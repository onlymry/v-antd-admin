export interface UserState {
  avatorUrl: string;
  departmentId: number;
  email: string;
  id: number;
  nickName: string;
  phone: string;
  rules: number;
  token: string;
  userName: string;
  islogin:boolean;
}

