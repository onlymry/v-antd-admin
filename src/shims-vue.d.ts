declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}


interface Window {
  T:any, // 天地图对象
  T_ANCHOR_TOP_LEFT:any //天地图常量
}

// mapbox
declare module './plugin/createMapBoxStreetsV6Style'
// json ??
declare module '@/assets/point/index.json';
declare module '@/assets/point/provi.json';
// openlayer 
declare module 'ol';
declare module 'ol/Map';
declare module 'ol/source/OSM';
declare module 'ol/layer/Tile';
declare module 'ol/View';
declare module 'ol/format/MVT';
declare module 'ol/tilegrid/TileGrid';
declare module 'ol/layer/VectorTile';
declare module 'ol/source/VectorTile';
declare module 'ol/style'
declare module 'ol/proj';
declare module 'ol/source/XYZ';
declare module 'ol/source/TileImage';
declare module 'ol/source';
declare module 'ol/format/GeoJSON';
declare module 'ol/layer';
declare module 'ol/format/EsriJSON';
declare module 'ol/source/Vector';
declare module 'ol/tilegrid';
declare module 'ol/loadingstrategy';
declare module 'ol/control';
declare module 'ol/interaction';
declare module 'ol/events/condition';
declare module 'ol/Feature';
declare module 'ol/geom/Point';
declare module 'ol/render';
declare module 'ol/easing';
declare module 'ol/Observable';
declare module 'ol/format';
declare module 'ol/geom/Circle';
declare module 'ol/Overlay';
declare module 'ol/geom';
declare module 'ol/OverlayPositioning';
// store
declare module 'store/src/store-engine';
declare module 'store/storages/localStorage';
declare module 'store/storages/sessionStorage';
declare module 'store/storages/cookieStorage';


// import OrbitControls from '';

declare module "three/examples/js/controls/OrbitControls.js";