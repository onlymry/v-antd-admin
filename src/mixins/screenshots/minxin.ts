import { Component, Mixins } from 'vue-property-decorator';
import html2canvas from 'html2canvas';

@Component
class screenShotsMixins extends Mixins() {
  // 截取图片
  public sheetScreen(htmlDom: any) {
    html2canvas(htmlDom).then((canvas) => {
      let image = this.convertCanvasToImage(canvas);
      let a = document.createElement('a');
      // 下载图片
      a.download = '地图.png';
      a.href = image.src;
      let event = new MouseEvent('click');
      a.dispatchEvent(event);
    });

  }
  // canvas 转 image
  convertCanvasToImage(canvas: any) {
    let image = new Image();
    image.src = canvas.toDataURL("image/png");
    return image;
  }
  // Converts image to canvas; returns new canvas element
  convertImageToCanvas(image: any) {
    let canvas: any = document.createElement("canvas");
    canvas.width = image.width;
    canvas.height = image.height;
    canvas.getContext("2d").drawImage(image, 0, 0);

    return canvas;
  }
}

export default screenShotsMixins