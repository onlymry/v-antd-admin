const routes = [
  {
    group: 'console',
    groupIcon: "user",
    title: "控制台",
    children: [
      {
        path: '/console',
        name: 'Console',
        meta: {
          auth: true,
          title: '控制台',
          icon: 'user',
        },
        component: () => import('@/views/pages/Console.vue')
      },
      {
        path: '/show-data',
        name: 'ShowData',
        meta: {
          auth: true,
          title: '数据展示',
          icon: 'radar-chart'
        },
        component: () => import('@/views/pages/ShowData.vue')
      },
      {
        path: "/g2",
        name: 'G2',
        meta: {
          auth: true,
          title: 'G2',
          icon: 'radar-chart'
        },
        component: () => import('@/views/pages/G2.vue')
      },

    ]
  },
  {
    group: 'opreations',
    groupIcon: "info-circle",
    title: "操作",
    children: [
      {
        path: '/from',
        name: 'From',
        meta: {
          auth: true,
          title: '操作表单',
          icon: 'form',
        },
        component: () => import('@/views/pages/From.vue')
      }
    ]
  },
  {
    group: 'document',
    groupIcon: "info-circle",
    title: "文档处理",
    children: [
      {
        path: "/execl",
        name: 'Execl',
        meta: {
          auth: true,
          title: 'Execl',
          icon: 'radar-chart'
        },
        component: () => import('@/views/pages/document/Execl.vue')
      },
      {
        path: "/pdf",
        name: 'Pdf',
        meta: {
          auth: true,
          title: 'Pdf',
          icon: 'radar-chart'
        },
        component: () => import('@/views/pages/document/Pdf.vue')
      },
      {
        path: "/word",
        name: 'Word',
        meta: {
          auth: true,
          title: 'Word',
          icon: 'radar-chart'
        },
        component: () => import('@/views/pages/document/Word.vue')
      },
      {
        path: "/rich-text",
        name: 'RichText',
        meta: {
          auth: true,
          title: '富文本',
          icon: 'radar-chart'
        },
        component: () => import('@/views/pages/document/RichText.vue')
      }
    ]


  },
  {
    group: 'tools',
    groupIcon: "redo",
    title: "工具",
    children: [
      {
        path: '/animate',
        name: 'Animate',
        meta: {
          auth: true,
          title: '动画-velocity',
          icon: 'redo',
        },
        component: () => import('@/views/pages/Animate.vue')
      },
      {
        path: '/anime',
        name: 'Anime',
        meta: {
          auth: true,
          title: '动画-anime',
          icon: 'redo',
        },
        component: () => import('@/views/pages/animate/anime.vue')
      },
      {
        path: '/store',
        name: 'store',
        meta: {
          auth: true,
          title: 'store',
          icon: 'redo',
        },
        component: () => import('@/views/pages/store.vue')
      }
    ]
  },
  {
    group: "map",
    groupIcon: "dribbble",
    title: "地图",
    children: [
      {
        path: '/openlayer',
        name: 'Openlayer',
        meta: {
          auth: true,
          title: 'Openlayer',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/map/Openlayer.vue')
      },
      {
        path: '/tianmap',
        name: 'TianMap',
        meta: {
          auth: true,
          title: '天地图',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/map/TianMap.vue')
      }
    ]
  },

  {
    group: "openlayer",
    groupIcon: 'desktop',
    title: "openlayer",
    children: [
      {
        path: '/base-openlayer',
        name: 'BaseOpenlayer',
        meta: {
          auth: true,
          title: 'BaseOpenlayer',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/base.vue')
      },
      {
        path: '/three-bird-openlayer',
        name: 'threeBirdMap',
        meta: {
          auth: true,
          title: '三方(mapbox)',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/threeBirdMap.vue')
      },

      {
        path: '/three-bird-gaode-openlayer',
        name: 'threeBirdGaoDeMap',
        meta: {
          auth: true,
          title: '三方(高德地图)',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/gaodeMap.vue')
      },
      {
        path: '/av',
        name: 'AdvancedView',
        meta: {
          auth: true,
          title: '高级视图定位',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/AdvancedView.vue')
      },
      {
        path: '/arcgis',
        name: 'ArcGIS',
        meta: {
          auth: true,
          title: 'ArcGIS',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/ArcGIS.vue')
      },
      {
        path: '/attributions',
        name: 'Attributions',
        meta: {
          auth: true,
          title: 'Attributions',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/Attributions.vue')
      },
      {
        path: '/bingmap',
        name: 'BingMap',
        meta: {
          auth: true,
          title: 'BingMap',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/BingMap.vue')
      },
      {
        path: '/box-selection',
        name: 'BoxSelection',
        meta: {
          auth: true,
          title: 'BoxSelection',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/BoxSelection.vue')
      },
      {
        path: '/point',
        name: 'Point',
        meta: {
          auth: true,
          title: '散点图',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/Point.vue')
      },
      {
        path: '/scalepoint',
        name: 'ScalePoint',
        meta: {
          auth: true,
          title: '根据层级变化点的扩大和缩小',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/ScalePoint.vue')
      },
      {
        path: '/openlayer-echarts',
        name: 'OpenlayerEcharts',
        meta: {
          auth: true,
          title: '在openlayer上画echarts',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/OpenlayerEcharts.vue')
      },
      {
        path: '/openlayer-antvg2',
        name: 'OpenlayerAntvG2',
        meta: {
          auth: true,
          title: '在openlayer上画antVG2',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/openlayer/OpenlayerAntvG2.vue')
      }
    ]
  },
  {
    group: "threejs",
    groupIcon: "desktop",
    title: "threejs",
    children: [
      {
        path: '/basethreejs',
        name: 'BaseThreejs',
        meta: {
          auth: true,
          title: '基础',
          icon: 'dribbble',
        },
        component: () => import('@/views/pages/threejs/base.vue')
      },
      
    ]
  }
];


const childrenRoutes: any[] = [

];

routes.forEach(ele => {
  childrenRoutes.push(...ele.children);
})


export default {
  childrenRoutes: childrenRoutes,
  routes: routes
};