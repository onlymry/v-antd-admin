import { RouteConfig } from 'vue-router';
import childrenRoutes from './children';
const routes: Array<RouteConfig> = [
  {
    path: "/",
    // 路由重定义，默认是home的第一个子级路由
    // 这里可以修改
    redirect: childrenRoutes.childrenRoutes[0].path
  },
  {
    path: '/home',
    name: 'Home',
    meta: { auth: true },
    component: () => import('@/views/Layout.vue'),
    children: childrenRoutes.childrenRoutes
  },
  {
    path: '/about',
    name: 'About',
    meta: { auth: true },
    component: () => import('@/views/About.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login.vue')
  },
  {
    path: '/notfound',
    name: "NotFound",
    component: () => import('@/views/NotFound.vue')
  }
];


export default routes;