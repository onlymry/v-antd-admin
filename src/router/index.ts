import Vue from 'vue'
import VueRouter from 'vue-router'
import routes from './config';

import NProgress from 'nprogress';
import 'nprogress/nprogress.css';

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes,
})

// 路由生命周期
router.beforeEach((to, from, next) => {
  NProgress.start()
  const isLogin = localStorage.getItem('isLogin') || null;
  if (to.meta.auth) {
    if (isLogin) {
      next();
    } else {
      next({
        path: '/login'
      })
    }
  } else {
    next();
  }

  if (to.fullPath == "/login") {
    if (isLogin) {
      next({
        path: from.fullPath
      });
    } else {
      next();
    }
  }
})


router.afterEach(() => {
  NProgress.done()
})

export default router
