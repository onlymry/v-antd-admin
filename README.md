## V-Antd-Admin
> 因为vue-antd-admin引入了一些我不太喜欢的玩意
> 所以使用TS+VUE+ANTD重构了这个版本
## 如何使用VAA
1. 菜单全都在`router`文件夹下面，`children`文件里面配置了所有侧边栏菜单，并作了分组；
使用示例如下：

```ts
{
    group:'opreations',
    groupIcon:"info-circle",
    title:"操作",
    children:[
      {
        path: '/from',
        name: 'From',
        meta: { 
          auth: true, 
          title: '操作表单', 
          icon: 'form',
        },
        component: () => import('@/views/pages/From.vue')
      }
    ]
  }
```
2. 在`tools`里面有一个`config.ts`有一个语法糖，这里可以配置项目是`dev`还是`product`,你可以在这里面添加相应的参数，比如连接本地调试时的`url`，线上的`url`等等。

3. 在`tools`中有一个`message.ts`这里面有一些通常使用的网络状态，你可以使用他们来为你的项目创建更好的用户交互。

## 使用的插件

|名称|依赖|功能|
--|:--:|--:
|g2|yarn add / npm i / cnpm i  @antv/g2 |图表|
|data-set|yarn add / npm i / cnpm i  @antv/data-set |图表|
|velocity-animate|yarn add / npm i / cnpm i  @types/velocity-animate |动画|
|lodash|yarn add / npm i / cnpm i @types/lodash|工具库|
|animejs|yarn add / npm i / cnpm i  @types/animejs |动画|
|jsonp|yarn add / npm i / cnpm i  @types/jsonp |ajax|
|nprogress|yarn add / npm i / cnpm i  nprogress |进度条|
|xlsx|yarn add / npm i / cnpm i  xlsx |Execl|
|ol|yarn add / npm i / cnpm i  ol |openlayer|
|axios|yarn add / npm i / cnpm i  axios |ajax|



## 工具类

```ts
export default class Tools {
  // 判断是否是浏览器环境
  public inBrowser = typeof window !== 'undefined';
  // 获取浏览器ua
  public UA = this.inBrowser && window.navigator.userAgent.toLowerCase();
  // 当前浏览器是否是ie
  public isIE = this.UA && /msie|trident/.test(this.UA);
  //当前浏览器是ie9
  public isIE9 = this.UA && this.UA.indexOf('msie 9.0') > 0;
  // 当前浏览器是edge
  public isEdge = this.UA && this.UA.indexOf('edge/') > 0;
  //当前浏览器是android
  public isAndroid = (this.UA && this.UA.indexOf('android') > 0);
  // 当前浏览器是ios
  public isIOS = (this.UA && /iphone|ipad|ipod|ios/.test(this.UA));
  // 当前浏览器是chrome
  public isChrome = this.UA && /chrome\/\d+/.test(this.UA) && !this.isEdge;
  // 当前环境是phantomjs
  public isPhantomJS = this.UA && /phantomjs/.test(this.UA);
  // 当前浏览器是火狐
  public isFF = this.UA && this.UA.match(/firefox\/(\d+)/);
  // 对象是否存在__proto__
  public hasProto = '__proto__' in {};
  // 判断变量是否是undefined
  public isUndef(v: any) {
    return v === undefined || v === null
  }
  //判断变量是否已经定义
  public isDef(v: any) {
    return v !== undefined && v !== null
  }
  // 判断变量是否为真
  public isTrue(v: any) {
    return v === true
  }
  // 判断变量是否为假
  public isFalse(v: any) {
    return v === false
  }
  // 判断变量是否是对象
  public isObject(obj: any) {
    return obj !== null && typeof obj === 'object'
  }
  // 判断变量是否是正则
  public isRegExp(v: any) {
    return Object.prototype.toString.call(v) === '[object RegExp]'
  }
  // 判断变量是否是Promise
  public isPromise(val: any) {
    return (
      this.isDef(val) &&
      typeof val.then === 'function' &&
      typeof val.catch === 'function'
    )
  }
  // 转化number
  public toNumber(val: any, type: string) {
    if (this.isUndef(type) || type === "float" || type === "double") {
      let n = parseFloat(val);
      return isNaN(n) ? val : n;
    } else {
      let n = parseInt(val);
      return isNaN(n) ? val : n;
    }
  }
}
```

## 编程思想

1. 能复制就复制，节约时间避免出错
1. 保留原本结构，简单上手容易调试
1. 增加必要注释，说明功能和使用方法
1. 说明随手可得，不用上网或打开文档
1. 命名必须规范，容易查找一看就懂
1. 重载尽量转发，减少代码容易修改
1. 最先校验参数，任意调用不会崩溃
1. 代码模块分区，方便浏览容易查找
1. 封装常用代码，方便使用降低耦合
1. 回收多余占用，优化内存提高性能
1. 分包结构合理，模块清晰浏览方便
1. 多用工具和快捷键，增删改查快捷高效